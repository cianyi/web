-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 2020-06-22 01:26:40
-- 伺服器版本： 10.4.11-MariaDB
-- PHP 版本： 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `classroom`
--

-- --------------------------------------------------------

--
-- 資料表結構 `form1`
--

CREATE TABLE `form1` (
  `id` int(11) NOT NULL,
  `url` text NOT NULL,
  `email` text NOT NULL,
  `tel` varchar(300) NOT NULL,
  `num` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `time` time NOT NULL,
  `point` int(11) NOT NULL,
  `color` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `form1`
--

INSERT INTO `form1` (`id`, `url`, `email`, `tel`, `num`, `birthday`, `time`, `point`, `color`) VALUES
(1, 'http://localhost/code/home/test', 'sgy6562365@yahoo.com.tw', '0912-345-678', 1, '0008-08-08', '08:08:00', 1, '#000000'),
(10, 'http://ssss', 'sgy6562365@gmail.com', 'ssaaaadadadasdasdasdasdasdasdasdasdasd', 13, '2020-06-11', '13:24:00', 5, '#d50b0b');

-- --------------------------------------------------------

--
-- 資料表結構 `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `s_name` varchar(64) DEFAULT NULL,
  `p_name` varchar(64) DEFAULT NULL,
  `address` varchar(128) DEFAULT NULL,
  `city` varchar(32) DEFAULT NULL,
  `state` char(2) DEFAULT NULL,
  `zip` char(10) DEFAULT NULL,
  `phone` char(20) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `student`
--

INSERT INTO `student` (`id`, `name`, `s_name`, `p_name`, `address`, `city`, `state`, `zip`, `phone`, `email`) VALUES
(9, '1', '1', '1', '111', '1', '1', '1', '1', '1'),
(10, '2', '8', '8', '8', '8', '8', '8', '8', '8'),
(12, 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a');

-- --------------------------------------------------------

--
-- 資料表結構 `users`
--

CREATE TABLE `users` (
  `u_id` int(80) NOT NULL,
  `account` varchar(20) NOT NULL,
  `password` varchar(30) NOT NULL,
  `name` text NOT NULL,
  `date` date NOT NULL,
  `address` text NOT NULL,
  `auth` int(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `users`
--

INSERT INTO `users` (`u_id`, `account`, `password`, `name`, `date`, `address`, `auth`) VALUES
(2, 'aaaa', '1234', '管理員帳號001', '2000-01-01', 'stu', 1),
(3, 'acaa', '1234', '普通人帳號002', '2000-02-02', 'stu', 0),
(11, 'abaa', '1234', '普通人帳號001', '2000-03-03', 'stu', 0);

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `form1`
--
ALTER TABLE `form1`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`u_id`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `form1`
--
ALTER TABLE `form1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `users`
--
ALTER TABLE `users`
  MODIFY `u_id` int(80) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
