<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class model extends CI_Model{
  // 新增資料
  function addData($data)
  {
    $this->db->insert('users', $data);
  }
  // 顯示資料
  function listData()
  {
    return $this->db->get('users');
  }
  // 取得資料
  function getData($id)
  {
  return $this->db->get_where('users', array('u_id'=> $id));
  }
  // 更新資料
  function updateData($id, $data)
  {
    $this->db->where('u_id', $id);
    // Microsoft SQL Server無法更新標識列"id"
    unset($data['u_id']);
    $this->db->update('users', $data);
  }
  // 刪除資料
  function deleteData($id)
  {
    $this->db->where('u_id', $id);
    $this->db->delete('users');
  }
}
?>