<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Student extends CI_Controller {
  function __construct()
  {
    parent::__construct();
    // load helpers
    $this->load->helper('url');
    $this->load->view('head/head');
  }
 
  function index()
  {
    $this->load->helper('form');
    $this->load->library('table');
  
    $this->load->model('MStudent','',TRUE);
    $students_qry = $this->MStudent->listStudents();
    // display information for the view
    $data['main_body'] = 'add_student';
    $data['include'] = 'add_student';
    $data['students_qry'] = $students_qry;
    $this->load->view('head/main',$data);
  }
  function add()
  {
    $this->load->helper('form');
   
    // display information for the view
    $data['main_body'] = 'student_add';
    $this->load->helper('url');
    $this->load->view('head/main',$data);
  }
  function create()
  {
    $this->load->helper('url');
    $data['main_body'] = 'student_edit';
    $this->load->model('MStudent','',TRUE);
    $this->MStudent->addStudent($_POST);
    redirect('student/index','refresh');
  }
  function listing()
  {
    $this->load->library('table');
    $data['main_body'] = 'student_listing';
    $this->load->model('MStudent','',TRUE);
    $students_qry = $this->MStudent->listStudents();
  
    // display information for the view
    $data['title'] = "Classroom: Student Listing";
    $data['headline'] = "Student Listing";
    $data['include'] = 'student_listing';
    $data['students_qry'] = $students_qry;
    $this->load->view('head/main',$data);
  }
  function edit()
  {
    $this->load->helper('form');
    $id = $this->uri->segment(3);
    $this->load->model('MStudent','',TRUE);
    $data['row'] = $this->MStudent->getStudent($id)->result();
    $data['main_body'] = 'student_edit';
    $data['include'] = 'student_edit';
    $this->load->helper('url');
    $this->load->view('head/main',$data);
  }
  function update()
  {
    $this->load->model('MStudent','',TRUE);
    $data['main_body'] = 'student_update';
    $this->MStudent->updateStudent($_POST['id'], $_POST);
    redirect('student/index','refresh');
  }
  function delete()
  {
    $id = $this->uri->segment(3);
    $data['main_body'] = 'student_edit';
    $this->load->model('MStudent','',TRUE);
    $this->MStudent->deleteStudent($id);
    redirect('student/listing','refresh');
  }
}
/* End of file student.php */
/* Location: ./application/controllers/student.php */