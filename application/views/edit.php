<?php if (!$this->session->userdata('account')) : ?>
<script type="text/javascript">
	alert("您尚未登入！");
	window.location.href = "http://localhost/web/";
</script>
<?php endif ?>
<?php if (!$this->session->userdata('auth') >= 1) : ?>
    <script type="text/javascript">
	alert("您權限不足");
	window.location.href = "http://localhost/web/";
</script>
<?php endif ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

echo form_open('home/update');
echo form_hidden('u_id', $row[0]->u_id);
$field_array = array('account','password','name','date','address','auth');
foreach($field_array as $field_name)
{
  echo '<p>' . $field_name.': ';
  echo form_input($field_name, $row[0]->$field_name) . '</p>';
}
echo form_submit('', 'Update',"onClick=\" return confirm('編輯成功')\"");
echo form_close();
?>