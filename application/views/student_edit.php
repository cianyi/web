<?php if (!$this->session->userdata('account')) : ?>
<script type="text/javascript">
	alert("您尚未登入！");
	window.location.href = "http://localhost/web/";
</script>
<?php endif ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

echo form_open('student/update');
echo form_hidden('id', $row[0]->id);
// an array of the fields in the student table
$field_array = array('name','s_name','p_name','address','city','state','zip','phone','email');
foreach($field_array as $field_name)
{
  echo '<p>' . $field_name.': ';
  echo form_input($field_name, $row[0]->$field_name) . '</p>';
}
// not setting the value attribute omits the submit from the $_POST array
echo form_submit('', 'Update',"onClick=\" return confirm('編輯成功')\"");
echo form_close();
?>