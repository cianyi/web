<?php if (!$this->session->userdata('account')) : ?>
<script type="text/javascript">
	alert("您尚未登入！");
	window.location.href = "http://localhost/web/";
</script>
<?php endif ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

echo form_open('student/create');
// an array of the fields in the student table
$field_array = array('name','s_name','p_name','address','city','state','zip','phone','email');
foreach($field_array as $field)
{
  echo '<p>' . $field.': ';
  echo form_input(array('name' => $field)) . '</p>';
}
// not setting the value attribute omits the submit from the $_POST array
echo form_submit('', 'Add',"onClick=\" return confirm('加入成功')\"");
echo form_close();
?>