<?php echo validation_errors(); ?>
<div class="container py-5">
	<div class="row align-items-end">
		<div class="d-flex justify-content-center w-100">
			<div class="text-center">
				<form action="<?php echo base_url("home/register_add")?>" method="post">
					<h1 class="text-center">註冊</h1>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1">帳號</span>
						</div>
						<input type="text" id="u_username" required="required"
							value="<?php echo set_value('u_account'); ?>" name="account" class="form-control"
							placeholder="輸入您的帳號" aria-label="account" aria-describedby="basic-addon1" minlength="4"
							maxlength="16">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon2">密碼</span>
						</div>
						<input type="password" id="u_password" required="required" name="password"
							value="<?php echo set_value('u_password'); ?>" class="form-control" placeholder="輸入您的密碼"
							aria-label="password" aria-describedby="basic-addon2" minlength="4" maxlength="16">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon4">姓名</span>
						</div>
						<input type="text" id="u_name" required="required" name="name" class="form-control"
							placeholder="輸入您的姓名" aria-label="password" aria-describedby="basic-addon4">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon5">生日</span>
						</div>
						<input type="date" id="u_date" required="required" name="date" class="form-control"
							aria-label="date" aria-describedby="basic-addon5">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon6">地址</span>
						</div>
						<input type="text" id="u_address" required="required" name="address" class="form-control"
							placeholder="輸入您的地址" aria-label="address" aria-describedby="basic-addon6">
					</div>
					<button type="submit" class="btn btn-secondary">註冊</button>
					<button type="button" class="btn btn-secondary"
						onclick="location.href='<?php echo base_url('') ?>'">回到登入</button>
				</form>
<<<<<<< HEAD
			</div>
			
=======
			</div>
>>>>>>> 3554ade87d56f1b6cd0dc698380142944b888a37
