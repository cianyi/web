<?php if ($this->session->userdata('account')) : ?>
	<style>
		* {
			margin: 0px;
			padding: 0px;
		}

		body {
			height: 100%;
		}

		div {
			background: #ddd;
			height: 50%;
		}
	</style>
	<div class="jumbotron jumbotron-fluid">
		<div class="container">
			<h1 class="display-4">首頁</h1>
			<p class="lead">歡　迎　來　到　練　習　系　統.</p>
		</div>
	</div>
<?php else : ?>
	<?php
	// redirect('login');
	?>
	<script type="text/javascript">
		alert("您尚未登入！");
		window.location.href = "http://localhost/code/";
	</script>
<?php endif ?>