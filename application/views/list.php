<?php if (!$this->session->userdata('account')) : ?>
<script type="text/javascript">
	alert("您尚未登入！");
	window.location.href = "http://localhost/web/";
</script>
<?php endif ?>
<?php if (!$this->session->userdata('auth') >= 1) : ?>
    <script type="text/javascript">
	alert("您權限不足");
	window.location.href = "http://localhost/web/";
</script>
<?php endif ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 // generate HTML table from query results
 $students_table = $this->table->generate($students_qry);
 
    // generate HTML table from query results
    $tmpl = array (
        'table_open' => '<table border="0" cellpadding="3" cellspacing="0">',
        'heading_row_start' => '<tr bgcolor="#66cc44">',
        'row_start' => '<tr bgcolor="#dddddd">'
        );
      $this->table->set_template($tmpl);
     
      $this->table->set_empty("&nbsp;");
   
      $this->table->set_heading('使用者帳號','使用者密碼', '使用者名稱', '使用者生日',
          '使用者地址','使用者權碼', ' 編輯/刪除 ');
   
      $table_row = array();
      foreach ($students_qry->result() as $student)
      {
        $table_row = NULL;
        //$table_row[] = anchor('student/edit/' . $student->id, 'edit');
        $table_row[] = $student->account;
        $table_row[] = $student->password;
        $table_row[] = $student->name;
        $table_row[] = $student->date;
        $table_row[] = $student->address;
        $table_row[] = $student->auth;
        $table_row[] = '<nobr>' .
        anchor('home/edit/' . $student->u_id, '編輯') . ' | ' .
        anchor('home/delete/' . $student->u_id, '刪除',
          "onClick=\" return confirm('你確定您要 '
            + '刪除Name為 $student->name 的資料嗎?')\"") .
        '</nobr>';
        $this->table->add_row($table_row);
      }   
      $students_table = $this->table->generate();
      echo $students_table;
?>