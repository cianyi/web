<?php if (!$this->session->userdata('account')) : ?>
<script type="text/javascript">
	alert("您尚未登入！");
	window.location.href = "http://localhost/web/";
</script>
<?php endif ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 // generate HTML table from query results
 $students_table = $this->table->generate($students_qry);
 
    // generate HTML table from query results
    $tmpl = array (
        'table_open' => '<table border="0" cellpadding="3" cellspacing="0">',
        'heading_row_start' => '<tr bgcolor="#66cc44">',
        'row_start' => '<tr bgcolor="#dddddd">'
        );
      $this->table->set_template($tmpl);
     
      $this->table->set_empty("&nbsp;");
   
      $this->table->set_heading('我的姓名','子女姓名', '父母姓名', '住家地址',
          '城市', '州', '郵遞區號', '電話', '電子郵件', '　功能');
   
      $table_row = array();
      foreach ($students_qry->result() as $student)
      {
        $table_row = NULL;
        //$table_row[] = anchor('student/edit/' . $student->id, 'edit');
        $table_row[] = $student->name;
        $table_row[] = $student->s_name;
        $table_row[] = $student->p_name;
        $table_row[] = $student->address;
        $table_row[] = $student->city;
        $table_row[] = $student->state;
        $table_row[] = $student->zip;
        $table_row[] = $student->phone;
        $table_row[] = mailto($student->email);
        $table_row[] = '<nobr>' .
        anchor('student/edit/' . $student->id, '編輯') . ' | ' .
        anchor('student/delete/' . $student->id, '刪除',
          "onClick=\" return confirm('你確定您要 '
            + '刪除Name為 $student->s_name 的資料嗎?')\"") .
        '</nobr>';
        $this->table->add_row($table_row);
      }   
      $students_table = $this->table->generate();
      echo $students_table;
?>