<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<title><?php echo $title;?></title>
</head>

<body>
	<div class="navigation">
		<?php
defined('BASEPATH') OR exit('No direct script access allowed');

  // nav bar
  echo (' | ');
  echo anchor('student/index', 'Home');
  echo (' | ');
  echo anchor('home/logout', 'Login Out',"onClick=\" return confirm('確定要登出嗎?')\"").'</nobr>';
  echo (' | ');
  if($this->session->userdata('account')){
    echo ($this->session->userdata('name'));
    echo ("，你好");
  }
  else{
    echo ("沒有登入");
  }
?>
	</div>
	<h1><?php echo $headline;?></h1>
	<?php $this->load->view($include);?>
</body>

</html>