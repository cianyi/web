<?php if (!$this->session->userdata('account')) : ?>
<script type="text/javascript">
	alert("您尚未登入！");
	window.location.href = "http://localhost/web/";
</script>
<?php endif ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

echo form_open('home/create');
$field_array = array('account','password','name','date','address','auth');
foreach($field_array as $field)
{
  echo '<p>' . $field.': ';
  echo form_input(array('name' => $field)) . '</p>';
}
echo form_submit('', 'Add',"onClick=\" return confirm('加入成功')\"");
echo form_close();
?>